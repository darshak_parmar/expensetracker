// import  { createElement } from "react";
import ExpenseItems from "./components/Expenses/ExpenseItems";
import "./App.css";
import NewExpenses from "./components/NewExpenses/NewExpenses";
import { useState } from "react";

const DUMMY_DATA = [
  {
    id: 1,
    title: "Car Insurance",
    date: new Date(2021, 3, 23),
    amount: 200.5,
  },
  {
    id: 2,
    title: "Zomato Food",
    date: new Date(2021, 6, 1),
    amount: 300.5,
  },
  {
    id: 3,
    title: "Petrol Lonawala Trip",
    date: new Date(2021, 6, 4),
    amount: 800.5,
  },
  {
    id: 4,
    title: "Car Tour",
    date: new Date(2021, 6, 4),
    amount: 300.5,
  },
  {
    id: 5,
    title: "Spiral Binding Book",
    date: new Date(2021, 6, 8),
    amount: 280.44,
  },
];

const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_DATA);
  // JSX Behind the sense
  // return createElement(
  //   "div",
  //   {},
  //   createElement("h1", {}, "Lets Start with ReactJs Library!"),
  //   createElement(ExpenseItems, { expensesData: expenses })
  // );
  const addExpenseDataHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div>
      <NewExpenses onAddExpense={addExpenseDataHandler} />
      <ExpenseItems expensesData={expenses} />
    </div>
  );
};

export default App;
