import React, { useState } from "react";
import Card from "../UI/Card";
import ExpensesFilter from "./ExpenseFilter/ExpenseFilter";
import ExpensesList from "./ExpensesList";
import "./ExpenseItems.css";
import ExpensesChart from "./ExpensesChart";

const ExpenseItems = (props) => {
  const [filterYear, setFilterYear] = useState("2020");
  const onSaveFilterYear = ($enteredYear) => {
    setFilterYear($enteredYear);
    console.log($enteredYear);
  };
  const filterYearDataContent = props.expensesData.filter((expense) => {
    return expense.date.getFullYear().toString() === filterYear;
  });

  return (
    <li>
      <Card className="expenses">
        <ExpensesFilter selected={filterYear} onFilterYear={onSaveFilterYear} />
        <ExpensesChart expenses={filterYearDataContent} />
        <ExpensesList filterData={filterYearDataContent} />
        {/* 
        ---------- Ternary Operator Conditioning -------
        {filterYearDataContent.length === 0 ? (
          <p>No Expenses Found!</p>
        ) : (
          filterYearDataContent.map((expense) => (
            <ExpenseItem
              key={expense.id}
              title={expense.title}
              date={expense.date}
              amount={expense.amount}
            />
          ))
        )} */}

        {/* 
        --------------- Some Trick to use conditions True && Executes -------------
        {filterYearDataContent.length === 0 && <p>No Expenses Found!</p>}
        {filterYearDataContent.length > 0 &&
          filterYearDataContent.map((expense) => (
            <ExpenseItem
              key={expense.id}
              title={expense.title}
              date={expense.date}
              amount={expense.amount}
            />
          ))} */}

        {/* 
        --------- Simple Rendering List through Map() ------------
        {props.expensesData.map((expense) => (
          <ExpenseItem
            key={expense.id}
            title={expense.title}
            date={expense.date}
            amount={expense.amount}
          />
        ))} */}

        {/* 
        --------- Filtering Year wise Expenses ------------------
        {props.expensesData
          .filter((expense) => {
            return (
              parseInt(filterYear) === parseInt(expense.date.getFullYear())
            );
          })
          .map((expense) => (
            <ExpenseItem
              key={expense.id}
              title={expense.title}
              date={expense.date}
              amount={expense.amount}
            />
          ))} */}

        {/* 
        ----------- Maping Filter Data through Array -----------
        {filterYearDataContent.map((expense) => (
          <ExpenseItem
            key={expense.id}
            title={expense.title}
            date={expense.date}
            amount={expense.amount}
          />
        ))} */}
      </Card>
    </li>
  );
};
export default ExpenseItems;
