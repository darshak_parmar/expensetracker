import React from "react";
import ExpenseItem from "./ExpenseItem";
import "./ExpensesList.css";

const ExpensesList = (props) => {
  if (props.filterData.length === 0) {
      return <h2 className="expenses-list__fallback">Expense Data Not Found!</h2>
  }
  return (
    <ul className="expenses-list">
      {props.filterData.map((expense) => (
        <ExpenseItem
          key={expense.id}
          title={expense.title}
          date={expense.date}
          amount={expense.amount}
        />
      ))}
    </ul>
  );
};
export default ExpensesList;
